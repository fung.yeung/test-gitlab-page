
with final as (
  SELECT
    file_id,
    sheet_name,
    business_unit,
    country,
    business_line,
    activity_type,
    project_name,
    site_name,
    globalreport_id,
    global_report_scope,
    climate_asset_scope,
    asia_code,
    global_report_code,
    indicator_name,
    indicator_type,
    activity,
    sub_activity,
    is_applicable,
    scope,
    unit,
    formula,
    time_step,
    raw_data_availability,
    interaction_w_other_reports,
    data_owner_email,
    period,
    `date`,
    budget,
    budget_f1,
    budget_f2,
    value,
    
    budget_comment,
    budget_f1_comment,
    budget_f2_comment,
    actual_comment as comment,
    -- for backward compatility, new dashboard should use "actual_comment"
    actual_comment,
    is_validated,
    "2023" as _ghg_type,
    insertion_time
  FROM
    `cn-ops-spdigital`.`asia_bsp`.`ghg_performance_2023`
)
select
  *
from
  final