
  
with final as (
  select
    file_id,
    sheet_name,
    business_unit,
    country,
    business_line,
    activity_type,
    project_name,
    site_name,
    globalreport_id,
    global_report_scope,
    climate_asset_scope,
    asia_code,
    global_report_code,
    indicator_name,
    indicator_type,
    activity,
    sub_activity,
    is_applicable,
    scope,
    unit,
    formula,
    time_step,
    raw_data_availability,
    interaction_w_other_reports,
    data_owner_email,
    period,
    `date`,
    CAST(NULL AS FLOAT64) as budget,
    CAST(NULL AS FLOAT64)  as budget_f1,
    CAST(NULL AS FLOAT64)  as budget_f2,
    value,
    
    CAST(NULL AS STRING) as budget_comment,
    CAST(NULL AS STRING) as budget_f1_comment,
    CAST(NULL AS STRING) as budget_f2_comment,
    comment,
    -- for backward compatility, new dashboard should use "actual_comment"
    comment as actual_comment,
    CAST(NULL AS BOOLEAN) as is_validated,
    "2022" as _ghg_type,
    insertion_time
  FROM
    `cn-ops-spdigital`.`asia_bsp`.`ghg_performance`
)
select
  *
from
  final