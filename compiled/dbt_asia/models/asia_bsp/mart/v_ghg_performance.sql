with union_2022_2023 as (
  select
    *
  from
    `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`stg_bsp__ghg_performance_2023`
  union all
  select
    *
  from
    `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`stg_bsp__ghg_performance`
),
add_helper_col as (
  select
    *,
    LAG(date) over (
      PARTITION BY business_unit,
      project_name,
      site_name,
      globalreport_id,
      asia_code,
      format_date(
        '%m',
        date
      ) -- month of current record
      ORDER BY
        date
    ) AS same_month_last_occurance_date,
    LAG(`value`) over (
      PARTITION BY business_unit,
      project_name,
      site_name,
      globalreport_id,
      asia_code,
      format_date(
        '%m',
        date
      )
      ORDER BY
        date
    ) AS same_month_last_occurance_value
  FROM
    union_2022_2023
  where
    period <> 'year'
),
final as (
  -- check if the last occurance is origniated from last year, if yes then take the value, if not return NULL
  select
    *
  except(
      same_month_last_occurance_date,
      same_month_last_occurance_value
    ),
    CASE
      WHEN extract(
        YEAR
        FROM
          same_month_last_occurance_date
      ) = (
        extract(
          YEAR
          from
            date
        ) - 1
      ) THEN same_month_last_occurance_value
      ELSE NULL
    END AS last_year_value
  FROM
    add_helper_col
)
select
  file_id,
  sheet_name,
  business_unit,
  country,
  business_line,
  activity_type,
  project_name,
  site_name,
  globalreport_id,
  global_report_scope,
  climate_asset_scope,
  asia_code,
  global_report_code,
  indicator_name,
  indicator_type,
  activity,
  sub_activity,
  is_applicable,
  scope,
  unit,
  formula,
  time_step,
  raw_data_availability,
  interaction_w_other_reports,
  data_owner_email,
  period,
  `date`,
  budget,
  budget_f1,
  budget_f2,
  value,
  last_year_value,
  budget_comment,
  budget_f1_comment,
  budget_f2_comment,
  comment,
  actual_comment,
  is_validated,
  _ghg_type,
  insertion_time
from
  final