SELECT
  site_code,
  site,
  FORMAT_DATE("%Y-%m", DATE_TRUNC(workorder_timestamp_scheduled_end, MONTH)) as date_month,
  COUNTIF(workorder_jobtype != 'MEC' AND workorder_class  = 'PS') as kpi_nb_preventive_wos, 
  COUNTIF(workorder_jobtype != 'MEC' AND workorder_class != 'PS') as kpi_nb_breakdown_wos,
  COUNTIF(workorder_jobtype != 'MEC') as kpi_nb_wos,
  COUNTIF(workorder_jobtype != 'MEC' AND workorder_class  = 'PS' AND workorder_status_level_1_name = "Completed" 
          ) as kpi_preventive_executions_completed_wos,
  COUNTIF(workorder_jobtype != 'MEC' AND workorder_class  = 'PS' AND workorder_status_level_1_name != "Completed" 
          ) as kpi_preventive_executions_notcompleted_wos,
  COUNTIF(workorder_jobtype != 'MEC' AND workorder_class  != 'PS' AND workorder_status_level_1_name = "Completed" 
          ) as kpi_breakdown_executions_completed_wos,
  COUNTIF(workorder_jobtype != 'MEC' AND workorder_class  != 'PS' AND workorder_status_level_1_name != "Completed" 
          ) as kpi_breakdown_executions_notcompleted_wos,
  current_timestamp() as insertion_timestamp

FROM `cn-ops-spdigital`.`maintenance`.`workorder`

WHERE workorder_timestamp_scheduled_end >= DATE_TRUNC(DATE_SUB(CURRENT_DATE(), INTERVAL 12 MONTH), MONTH) -- beginning of last month
  AND workorder_timestamp_scheduled_end <= DATE_SUB(DATE_TRUNC(CURRENT_DATE(), MONTH), INTERVAL 1 DAY) -- end of last  month
  AND organization_code = "STH"
  AND site_code != '1-00001' -- filter out T-Park

GROUP BY site_code, site, date_month
ORDER BY site_code, date_month desc