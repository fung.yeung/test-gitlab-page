SELECT
  site_code,
  site,
  workorder_timestamp_scheduled_end,
  workorder_id, 
  workorder_description,
  workorder_status_level_1_name,
  current_timestamp() as insertion_timestamp

  FROM `cn-ops-spdigital`.`maintenance`.`workorder`

WHERE workorder_timestamp_scheduled_end >= DATE_TRUNC(DATE_SUB(CURRENT_DATE(), INTERVAL 12 MONTH), MONTH) -- 12 months ago
  AND workorder_timestamp_scheduled_end <= DATE_SUB(DATE_TRUNC(CURRENT_DATE(), MONTH), INTERVAL 1 DAY) -- end of last  month
  AND organization_code              = "STH"
  AND site_code                     != '1-00001' -- filter out T-Park
  AND workorder_jobtype             != 'MEC'
  AND workorder_status_level_1_name IN ('Prepared','Lack of information')
ORDER BY site_code, workorder_timestamp_scheduled_end desc