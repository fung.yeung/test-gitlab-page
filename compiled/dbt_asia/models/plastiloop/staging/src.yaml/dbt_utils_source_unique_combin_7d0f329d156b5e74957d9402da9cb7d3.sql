





with validation_errors as (

    select
        entity_id, indicator_asia_id, period_start, period_end
    from `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`finance_raw_data_cn`
    group by entity_id, indicator_asia_id, period_start, period_end
    having count(*) > 1

)

select *
from validation_errors


