





with validation_errors as (

    select
        site_id_operational, product_local_name, client_local_name, keyaccount_code, period_start, period_end
    from `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`commercial_raw_data_cn`
    group by site_id_operational, product_local_name, client_local_name, keyaccount_code, period_start, period_end
    having count(*) > 1

)

select *
from validation_errors


