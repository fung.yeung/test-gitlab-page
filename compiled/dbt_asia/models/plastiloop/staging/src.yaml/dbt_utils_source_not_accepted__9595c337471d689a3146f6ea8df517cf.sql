
with all_values as (

    select distinct
        product_local_name as value_field

    from `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`commercial_raw_data_cn`

),

validation_errors as (

    select
        value_field

    from all_values
    where value_field in (
        'N/A'
        )

)

select *
from validation_errors

