

with final as (

  select
    finance.entity_id,
    finance.indicator_asia_id,
    finance.period_start,
    finance.period_end,
    finance.unit_code,
    finance.actual_value,
    finance.budget_value,
    finance.actual_ytd_value,
    finance.budget_ytd_value,
    finance.actual_value_comment,
    finance.budget_value_comment,
    finance.is_validated,
    finance.insertion_time,
    indicator.name,
    indicator.definition,
    indicator.is_mandatory,
    indicator.domain,
    indicator.category,
    indicator.domain_source_application,
    indicator.expected_unit,
    indicator.level,
    indicator.calculation_formula,
    indicator.aggregation,
    entity.link_finance_or_operation,
    entity.site_id_operational,
    entity.site_id_financial,
    entity.site_id_globalreport,
    entity.bu_code,
    entity.country_code,
    entity.local_currency,
    entity.business_segment_id,
    entity.operation_date_start
  from
    `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`finance_raw_data_cn` as finance
    left join `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`ref_indicators_finance` indicator
    on finance.indicator_asia_id = indicator.id
    left join `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`ref_entity` as entity
    on finance.entity_id = entity.site_id_financial
)
select
  *
from
  final