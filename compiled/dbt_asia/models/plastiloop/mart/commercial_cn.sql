

with final as (

  select
    commercial.site_id_operational,
    commercial.product_local_name,
    commercial.plastiloop_product_code,
    commercial.client_local_name,
    commercial.keyaccount_code,
    commercial.sales_tons,
    commercial.sales_revenues,
    commercial.currency,
    commercial.period_start,
    commercial.period_end,
    commercial.comment,
    commercial.is_validated,
    entity.link_finance_or_operation,
    entity.site_id_financial,
    entity.site_id_globalreport,
    entity.bu_code,
    entity.country_code,
    entity.local_currency,
    entity.business_segment_id,
    
    grades.industry,
    grades.series,
    grades.grade,
    grades.plastiloop_grade,
    grades.resin,
    grades.shape,
    grades.processing_method,
    keyaccounts.keyaccount_name,
    keyaccounts.keyaccount_type,
    keyaccounts.market_segment
  from
    `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`commercial_raw_data_cn` as commercial
    left join `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`ref_entity` as entity
    on commercial.site_id_operational = entity.site_id_operational
    left join `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`ref_plastiloop_grades` as grades
    on commercial.plastiloop_product_code = grades.plastiloop_code
    left join `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`ref_keyaccounts` as keyaccounts
    on commercial.keyaccount_code = keyaccounts.keyaccount_code
)
select
  *
from
  final