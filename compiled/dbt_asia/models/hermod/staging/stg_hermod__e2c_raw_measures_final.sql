with final as (
  select
    *,
    `timestamp` as e2c_timestamp,
    datetime(
      `timestamp`,
      'Asia/Singapore'
    ) as `sg_time`,
    row_number() over (
      partition by timeserie_name
      order by
        `timestamp` desc
    ) as rn
  from
    `sg-ist-singapore-datadesk`.`00787_fomeasurev1_singapore`.`e2c_raw_measures_final` as e2c 
)
select
  *
from
  final