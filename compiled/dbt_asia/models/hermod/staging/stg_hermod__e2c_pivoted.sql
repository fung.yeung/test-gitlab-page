with e2c as (
    -- filter timestamp and timeserie_name required for calculation
    select
        timeserie_name,
        sg_time,
        e2c_timestamp,
        value
    from
        `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`stg_hermod__e2c_raw_measures_final`
    where
        1 = 1 
        and timeserie_name in (
            -- 302101301,
            '01-PISA-201',
            '01-PICA-007',
            -- 304202301
            '01-PIA-401',
            -- 302303301
            '01-PIA-404',
            -- 302502301, 302401301
            '01-PI-451A',
            '01-PI-451C',
            -- 302301301
            '01-PIA-452',
            '01-PIA-502',
            -- 302601301
            '01-PISA-505'
        )
),
pivot as (
    select
        *
    from
        e2c pivot (sum(value) for timeserie_name in ('01-PISA-201', '01-PICA-007', '01-PIA-401', '01-PIA-404', '01-PI-451A', '01-PI-451C', '01-PIA-452', '01-PIA-502', '01-PISA-505'))
)
select
    *
from
    pivot