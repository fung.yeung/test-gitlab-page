with final as (
    select
        mapping.site_id,
        mapping.characteristic,
        mapping.line_number,
        mapping.stream_index,
        mapping.stream_kind,
        mapping.stream_step_index,
        mapping.stream_step_kind,
        mapping.external_id,
        mapping.unit,
        e2c.timeserie_name,
        e2c.value,
        sg_time,
        e2c_timestamp,
        -- for filtering,
        mapping._primary_key
    from
        `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`stg_hermod__e2c_raw_measures_final` as e2c
        inner join `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`materalized_hermod_mapping` as mapping
        on mapping._e2c_timeserie_name = e2c.timeserie_name
)
select
    *
from
    final