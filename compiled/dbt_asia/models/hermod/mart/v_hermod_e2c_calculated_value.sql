
with transform as (
    -- compute required measure point
    select
        sg_time,
        e2c_timestamp,
        abs(
            `01-PISA-201` - `01-PICA-007`
        ) as `302101301`,
        abs(
            `01-PISA-201` - `01-PIA-401` * 0.01
        ) as `304202301`,
        abs((`01-PIA-401` - `01-PIA-404`) * 0.01) as `302303301`,
        abs(
            `01-PIA-404` - ((`01-PI-451A` + `01-PI-451C`) / 2) * 0.01
        ) as `302502301`,
        abs(((`01-PI-451A` + `01-PI-451C`) / 2) - `01-PIA-452` * 0.01) as `302401301`,
        abs((`01-PIA-452` - `01-PIA-502`) * 0.01) as `302301301`,
        abs((`01-PIA-502` - `01-PISA-505`) * 0.01) as `302601301`
    from
        `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`stg_hermod__e2c_pivoted`
        
),
unpivot_all_data as (
    -- unpivot the measure_points back to row and ready to join with mapping tbl
    select
        _primary_key,
        sg_time,
        e2c_timestamp,
        value
    from
        transform unpivot (
            value for `_primary_key` in (
                `302101301`,
                `304202301`,
                `302303301`,
                `302502301`,
                `302401301`,
                `302301301`,
                `302601301`
            )
        )
),
-- get last available record for each _primary_key
unpivot_add_rn as (
    select
        _primary_key,
        sg_time,
        e2c_timestamp,
        value,
        row_number() over(
            partition by `_primary_key`
            order by
                sg_time desc
        ) as rn
    from
        unpivot_all_data
),
unpivot as (
    -- rows can have different timestamp because there is time diff on when they landed in BQ and available for computation
    select
        _primary_key,
        sg_time,
        e2c_timestamp,
        value,
    from
        unpivot_add_rn 
),
final as (
    -- join calculated table with mapping
    select
        mapping.site_id,
        mapping.characteristic,
        mapping.line_number,
        mapping.stream_index,
        mapping.stream_step_index,
        mapping.stream_kind,
        mapping.stream_step_kind,
        mapping.external_id,
        mapping.unit,
        unpivot.`value`,
        unpivot.sg_time,
        unpivot.e2c_timestamp,
        unpivot._primary_key
    from
        unpivot
        inner join `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`materalized_hermod_mapping`
        
        as mapping
        on unpivot._primary_key = mapping._primary_key
)
select
    *
from
    final