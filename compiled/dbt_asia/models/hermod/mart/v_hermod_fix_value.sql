with final as (
    select
        mapping.site_id,
        mapping.characteristic,
        mapping.line_number,
        mapping.stream_index,
        mapping.stream_kind,
        mapping.stream_step_index,
        mapping.stream_step_kind,
        mapping.external_id,
        mapping.unit,
        mapping.measure_value,
        
        mapping._primary_key,
    from
        `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`materalized_hermod_mapping` as mapping
    where
        mapping.measure_value is not null
)
select
    *
from
    final