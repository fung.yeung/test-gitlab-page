with csv_format_raw_plus_calculated as (
  select
    site_id as siteid,
    characteristic as characteristic,
    line_number as linenumber,
    stream_index as streamindex,
    stream_kind as streamkind,
    stream_step_index as streamstepindex,
    stream_step_kind as streamstepkind,
    external_id as externalid,
    unit as unit,
    `value` as measureval,
    sg_time as `Timestamp`,
    e2c_timestamp
  from
    `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`v_hermod_e2c_raw_value`
  union all
  select
    site_id as siteid,
    characteristic as characteristic,
    line_number as linenumber,
    stream_index as streamindex,
    stream_kind as streamkind,
    stream_step_index as streamstepindex,
    stream_step_kind as streamstepkind,
    external_id as externalid,
    unit as unit,
    `value` as measureval,
    sg_time as `Timestamp`,
    e2c_timestamp
  from
    `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`v_hermod_e2c_calculated_value`
),
distinct_e2c_ts AS (
  SELECT
    distinct e2c_timestamp,
    `Timestamp`
  FROM
    -- `cn-ops-spdigital-dev-dev.dbt_fung_asia_southeast.v_hermod_e2c_calculated_value`
    -- `cn-ops-spdigital-dev-dev.dbt_fung_asia_southeast.v_hermod_e2c_raw_value`
    csv_format_raw_plus_calculated -- `cn-ops-spdigital-dev-dev.dbt_fung_asia_southeast.v_hermod_fix_value` as fix
),
generate_fix_value_rows as (
  SELECT
    site_id,
    characteristic,
    line_number,
    stream_index,
    stream_kind,
    stream_step_index,
    stream_step_kind,
    external_id,
    unit,
    measure_value,
    `Timestamp`,
    e2c_timestamp
  FROM
    `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`v_hermod_fix_value`
    CROSS JOIN distinct_e2c_ts
),
select
  site_id as siteid,
  characteristic as characteristic,
  line_number as linenumber,
  stream_index as streamindex,
  stream_kind as streamkind,
  stream_step_index as streamstepindex,
  stream_step_kind as streamstepkind,
  external_id as externalid,
  unit as unit,
  measure_value as measureval,
  `Timestamp`,
  e2c_timestamp
from
  generate_fix_value_rows
UNION all
select
  *
from
  csv_format_raw_plus_calculated