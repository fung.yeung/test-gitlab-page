with combined as (
    -- combined = raw_value + calculated_value
    select
        site_id as siteid,
        characteristic as characteristic,
        line_number as linenumber,
        stream_index as streamindex,
        stream_kind as streamkind,
        stream_step_index as streamstepindex,
        stream_step_kind as streamstepkind,
        external_id as externalid,
        unit as unit,
        `value` as measureval,
        sg_time as `Timestamp`,
        e2c_timestamp,
        _primary_key,
    from
        `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`v_hermod_e2c_raw_value`
    union all
    select
        site_id as siteid,
        characteristic as characteristic,
        line_number as linenumber,
        stream_index as streamindex,
        stream_kind as streamkind,
        stream_step_index as streamstepindex,
        stream_step_kind as streamstepkind,
        external_id as externalid,
        unit as unit,
        `value` as measureval,
        sg_time as `Timestamp`,
        e2c_timestamp,
        _primary_key
    from
        `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`v_hermod_e2c_calculated_value`
),
combine_add_rn as (
    select
        *,
        row_number() over (
            partition by _primary_key
            order by
                e2c_timestamp desc
        ) as rn
    from
        combined
    where
        e2c_timestamp >= timestamp_sub(current_timestamp(), interval 1 hour)),
        combine_latest as (
            select
                *
            except(
                    rn,
                    _primary_key
                )
            from
                combine_add_rn
            where
                rn = 1
        ),
        final as (
            select
                *
            except(e2c_timestamp)
            from
                combine_latest
            union all
                -- raw value timestamp relies on other two views
            select
                site_id as siteid,
                characteristic as characteristic,
                line_number as linenumber,
                stream_index as streamindex,
                stream_kind as streamkind,
                stream_step_index as streamstepindex,
                stream_step_kind as streamstepkind,
                external_id as externalid,
                unit as unit,
                measure_value as measureval,
                (
                    select
                        max(`Timestamp`)
                    from
                        combine_latest
                ) as `Timestamp`
            from
                `cn-ops-spdigital-dev-dev`.`plastiloop_monitoring`.`v_hermod_fix_value`
        )
    select
        *
    from
        final