with
  final as (
    select
    USR_DESC,
    UOG_USER,
    USR_CODE,
    UOG_ORG,
    /*CASE 
        WHEN ORG_temp is null or ORG_temp="*" THEN UOG_ORG 
    ELSE ORG_temp
    END AS ORG,*/
    USR_GROUP,
    UOG_ROLE,
    from
      `cn-ops-spdigital-dev-dev`.`dbt_staging`.`stg_amaas_user__raw_user` as user
    left join
      `cn-ops-spdigital-dev-dev`.`dbt_staging`.`stg_amaas_user__raw_user_org` as user_org
      on UOG_USER = USR_CODE 
  )
SELECT
  DISTINCT *
FROM
  final