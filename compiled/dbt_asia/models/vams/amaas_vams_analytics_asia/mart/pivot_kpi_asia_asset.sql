with
  final as (
    select
      *
    from
      `cn-ops-spdigital-dev-dev`.`dbt_staging`.`stg_amaas_asset__fo_asset` as asset
    left join
      `cn-ops-spdigital-dev-dev`.`dbt_staging`.`stg_amaas_asset__fo_work_order` as wo
      on wo.Workorder_Asset_gid = asset.asset_gid
  )
SELECT
  DISTINCT *
FROM
  final