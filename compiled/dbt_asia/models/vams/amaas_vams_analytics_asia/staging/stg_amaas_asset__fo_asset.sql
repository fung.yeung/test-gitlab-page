with asset_get_org as (
  -- get column Asset_ORG for final table join
  SELECT
    *,
    SPLIT(asset_id,'#')[ORDINAL(1)] Asset_ORG,
    functional_location_gid AS Asset_Level,     
  FROM
    `cn-ops-spdigital`.`00854_FO_OPERATION_ASIA`.`FO_ASSET_V1`
  )
  , final as (
  -- renaming columns value
  SELECT
    DISTINCT 
    asset_id,
    asset.asset_gid,
    asset_name,
    asset_type_code,
    Asset_ORG,
    Asset_Level,
    asset.functional_location_gid,
    asset_date_commissionning,
    asset_date_closing,
    asset_status_group_code,
    asset_status_group_name,
  
--Done    --AssetStatus
    CASE
      WHEN ( asset_date_closing IS NOT NULL )
    THEN "Closed"
    ELSE
    "Opened"
  END
    AS Asset_Status,
    -- wo.asset_gid AS Workorder_Asset_gid,
--Review    --AssetIsMaintained
  --   CASE
  --     WHEN ( wo.asset_gid IS NOT NULL) -- Si l'Asset_id existe dans FO_Workorder, Il est donc maintenu sinon non.
  --   THEN "Yes"
  --   ELSE
  --   "No"
  -- END
  --   AS Asset_Is_Maintained,
--Done    ---Asset_Is_Condition_Assessed : Boolean
    CASE
      WHEN ( asset_maintenance_condition_code IS NOT NULL ) THEN "Yes"
    ELSE
    "No"
  END
    AS Asset_Is_Condition_Assessed,
    
    asset_maintenance_condition_code,
    asset_maintenance_condition_name,
    
--Done    ---Asset_Is-Critical : Boolean
    CASE
      WHEN (asset_criticality_code IN ("1", "2", "3", "4","5")) THEN "Yes"
    ELSE
    "No"
  END
    AS Asset_Is_Critical,
    asset_criticality_code,
    asset_criticality_name,
    
--Done    ---Asseet_Is_risk_Assessed : Boolean
    CASE
      WHEN (asset_maintenance_condition_code IS NOT NULL AND asset_criticality_code IN ("1", "2", "3", "4","5")) --Les deux champs ne doivent pas être NULL
    THEN "Yes"
    ELSE
    "No"
  END
    AS Asset_Is_risk_Assessed,
    
--Review  ---RISK   
    CASE
      WHEN ( 
        (asset_criticality_code IN ("1") AND asset_maintenance_condition_code IN ('1', '2', '3')) 
      OR (asset_criticality_code IN ('2') AND asset_maintenance_condition_code IN ('1', '2')) 
      OR (asset_criticality_code IN ('3') AND asset_maintenance_condition_code IN ('1')) 
      ) 
      THEN "Acceptable"

      WHEN ( 
        (asset_criticality_code IN ('1')
        AND asset_maintenance_condition_code IN ('4','5'))
      OR (asset_criticality_code IN ('2')
        AND asset_maintenance_condition_code IN ('3','4'))
      OR (asset_criticality_code IN ('3')
        AND asset_maintenance_condition_code IN ('2'))
         ) 
        THEN "Moderate" 

        WHEN( (asset_criticality_code IN ("2") AND asset_maintenance_condition_code IN ('5')) 
        OR (asset_criticality_code IN ('3') AND asset_maintenance_condition_code IN ('3','4','5'))
        ) 
        THEN "Unacceptable"    
    ELSE
    "Not Assessed"
    END
    AS RISK,
    FROM
      asset_get_org as ASSET  
  )
select * from final



/*
Fung: original excerpt from 
-- WITH
--     LAST_FO_ASSET AS (
--    WITH
--       FO_ASSET AS (
--       SELECT
--         * EXCEPT(_rank)
--       FROM (
--         SELECT
--           *,
--           RANK() OVER (PARTITION BY asset_gid ORDER BY source_system_update_time DESC) _rank
--         FROM
--           `cn-ops-spdigital`.`00854_FO_OPERATION_ASIA`.`FO_ASSET_V1` )
--       WHERE
--         _rank = 1 )
--     SELECT
--       *,
--       SPLIT(asset_id,'#')[ORDINAL(1)] Asset_ORG,
--       functional_location_gid AS Asset_Level,     
--     FROM
--       `cn-ops-spdigital`.`00854_FO_OPERATION_ASIA`.`FO_ASSET_V1`
--     WHERE
--       asset_type_code = 'VEQ'
--       AND asset_status_group_code <>'EO'
--       )
*/