-- deduplication
WITH
  r5_user_organization_add_rn as (
        SELECT 
            *
            , row_number() OVER (
            PARTITION BY UOG_USER,UOG_ORG,UOG_ROLE 
            ORDER BY UOG_LASTSAVED DESC
        ) AS rn
        FROM 
            `gbl-imt-ve-datalake-prod`.`00504_vams_asia`.`V_R5USERORGANIZATION`    
    ), r5_user_organization as (
        select * except(rn)
        FROm r5_user_organization_add_rn
        where rn =1
    )
select * from r5_user_organization