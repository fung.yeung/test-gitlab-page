-- deduplication
WITH
  r5_users_add_rn AS ( 
      SELECT 
          *
          , row_number() OVER (
          PARTITION BY  
              USR_CODE
          ORDER BY
              USR_LASTSAVED DESC
      ) AS rn
      FROM 
        `gbl-imt-ve-datalake-prod`.`00504_vams_asia`.`V_R5USERS`
  )
  , r5_users AS ( 
      select * except(rn)
      FROM r5_users_add_rn
      WHERE rn=1
  )
select * from r5_users