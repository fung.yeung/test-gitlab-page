with final as (
    select
        wo.asset_gid AS Workorder_Asset_gid,
    -- Review    --AssetIsMaintained
        CASE
        WHEN ( wo.asset_gid IS NOT NULL) -- Si l'Asset_id existe dans FO_Workorder, Il est donc maintenu sinon non.
        THEN "Yes"
        ELSE
        "No"
    END
        AS Asset_Is_Maintained,
        from `cn-ops-spdigital`.`00854_FO_OPERATION_ASIA`.`FO_WORK_ORDER_V1` as wo
    -- Done    ---Asset_Is_Condition_Assessed : Boolean
)
select * from final